#+TITLE: My Emacs Configuration
#+AUTHOR: Alessio Caiazza
#+STARTUP: showall
#+PROPERTY: header-args:emacs-lisp :results output silent
#+EXPORT_FILE_NAME: index

This is [[https://abisso.org][my]] Emacs configuration, it's not complete yet.

I was using spacemacs and this is an attempt to write my emacs
config from scratch.

* Bootstrap

** =init.el=
   This is not an article about my emacs config, it's the real configuration file.

   In order to load it, it must self-generate a very simple =~/.emacs.d/init.el=

  #+BEGIN_SRC emacs-lisp :var my-emacs-config=(buffer-file-name) :tangle ~/.emacs.d/init.el :mkdirp yes
    (setq nolith/emacs-config-file my-emacs-config)
    (org-babel-load-file my-emacs-config)
  #+END_SRC

** set a custom file
   Because my =init.el= is tangled it will lose the
   =custom-set-variables= every time I tangle my config.

   Not that I use custom at all, but local variables needs a custom
   file to store safe values.  Hence we want to load them from a
   specific file, and make sure that file exists before loading it.

   #+begin_src emacs-lisp
     (defconst custom-file (expand-file-name "custom.el" user-emacs-directory))
     (unless (file-exists-p custom-file)
       (write-region ";; My custom file" nil custom-file))
     (load custom-file)
   #+end_src

*** DONE do not hardcode =emacs.org= path.

** Straight.el
   My package manager is [[https://github.com/raxod502/straight.el][=straight.el=]], it needs a bootstrapping code.

   Before bootstrapping, let's make sure we can [[https://github.com/raxod502/straight.el#how-do-i-pin-package-versions-or-use-only-tagged-releases][pin packages to specific versions]].

   #+begin_src emacs-lisp :var my-emacs-folder=(file-name-directory buffer-file-name)
     (let* ((version-folder (expand-file-name "versions" my-emacs-folder))
	    (default-profile (expand-file-name "default.el" version-folder))
	    (pinned-profile (expand-file-name "pinned.el" version-folder)))
       (setq straight-profiles '())
       (add-to-list 'straight-profiles (cons 'pinned pinned-profile))
       (add-to-list 'straight-profiles (cons 'nil default-profile)))
   #+end_src


   #+BEGIN_SRC emacs-lisp
     (defvar bootstrap-version)
     (let ((bootstrap-file
	    (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
	   (bootstrap-version 5))
       (unless (file-exists-p bootstrap-file)
	 (with-current-buffer
	     (url-retrieve-synchronously
	      "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	      'silent 'inhibit-cookies)
	   (goto-char (point-max))
	   (eval-print-last-sexp)))
       (load bootstrap-file nil 'nomessage))
  #+END_SRC

  Now we need to load =staight-x= for the experimental pinning support.

  #+begin_src emacs-lisp
    (load "straight-x")
  #+end_src

  I'm using =use-package= to load and configure my packages,
  let's make it use =straight.el= by default.

  #+BEGIN_SRC emacs-lisp
  (straight-use-package 'use-package)
  (setq straight-use-package-by-default 't)
  #+END_SRC

* Better defaults

No toolbar and scrollbar

#+BEGIN_SRC emacs-lisp
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
#+END_SRC

No splash screen

#+BEGIN_SRC emacs-lisp
(setq inhibit-startup-message t)
#+END_SRC

Answering just 'y' or 'n' will do

#+BEGIN_SRC emacs-lisp
(defalias 'yes-or-no-p 'y-or-n-p)
#+END_SRC

Highlight matching parentheses when the point is on them.

#+BEGIN_SRC emacs-lisp
(show-paren-mode 1)
#+END_SRC

Dired on MacOS requires =coreutils=.

#+name: install_coreutils
#+begin_src shell :tangle no :results output silent
  if test (uname) = "Darwin"
      brew list coreutils >/dev/null 2>&1 || brew install coreutils
  end
#+end_src
#+call: install_coreutils()

Source [[https://jonathanabennett.github.io/blog/2019/06/05/file-management-in-emacs-with-dired-mode/][this blog post]].

#+begin_src emacs-lisp
  (use-package dired
    :straight nil
    :config
    (when (string= system-type "darwin")
      (setq dired-use-ls-dired t
	    insert-directory-program "/usr/local/bin/gls"))
    :custom
    (dired-listing-switches "-aBhl --group-directories-first"))
#+end_src

And I don't want to use Tabs

#+begin_src emacs-lisp
  (setq-default indent-tabs-mode nil)
#+end_src


** Autosaves and backups

   =emacs= likes to clobber your working dir with backups files, they have a =~= at the end.

   I like my backups file to live under emacs directory

#+begin_src emacs-lisp
  (defvar --backup-directory (concat user-emacs-directory "backups"))
  (if (not (file-exists-p --backup-directory))
	  (make-directory --backup-directory t))
  (setq backup-directory-alist `(("." . ,--backup-directory)))
#+end_src

#+begin_src emacs-lisp
  (setq make-backup-files t               ; backup of a file the first time it is saved.
	backup-by-copying t               ; don't clobber symlinks
	version-control t                 ; version numbers for backup files
	delete-old-versions t             ; delete excess backup files silently
	delete-by-moving-to-trash t
	kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
	kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
	auto-save-default t               ; auto-save every buffer that visits a file
	auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
	auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
	)
#+end_src

* Custom functions

** =comment-or-uncomment-line-or-region=

   This function will comment or uncomment a region if selected, if not it will comment or uncomment the current line.

   #+begin_src elisp
     (defun comment-or-uncomment-line-or-region ()
       "Comments or uncomments the current line or region."
       (interactive)
       (if (region-active-p)
	   (comment-or-uncomment-region (region-beginning) (region-end))
	 (comment-or-uncomment-region (line-beginning-position) (line-end-position))
	 )
       )
   #+end_src

** =rename-current-buffer-file=

   This function renames the current open buffer, it came from [[https://github.com/syl20bnr/spacemacs/blob/bd7ef98e4c35fd87538dd2a81356cc83f5fd02f3/layers/%2Bdistributions/spacemacs-base/funcs.el#L294][spacemacs]].

   #+begin_src elisp
     (defun nolith/rename-current-buffer-file ()
       "Renames current buffer and file it is visiting."
       (interactive)
       (let* ((name (buffer-name))
	     (filename (buffer-file-name)))
	 (if (not (and filename (file-exists-p filename)))
	     (error "Buffer '%s' is not visiting a file!" name)
	   (let* ((dir (file-name-directory filename))
		  (new-name (read-file-name "New name: " dir)))
	     (cond ((get-buffer new-name)
		    (error "A buffer named '%s' already exists!" new-name))
		   (t
		    (let ((dir (file-name-directory new-name)))
		      (when (and (not (file-exists-p dir)) (yes-or-no-p (format "Create directory '%s'?" dir)))
			(make-directory dir t)))
		    (rename-file filename new-name 1)
		    (rename-buffer new-name)
		    (set-visited-file-name new-name)
		    (set-buffer-modified-p nil)
		    (when (fboundp 'recentf-add-file)
			(recentf-add-file new-name)
			(recentf-remove-if-non-kept filename))
		    (when (projectile-project-p)
		      (call-interactively #'projectile-invalidate-cache))
		    (message "File '%s' successfully renamed to '%s'" name (file-name-nondirectory new-name))))))))
   #+end_src

* Appearence
  I like the [[https://github.com/bbatsov/zenburn-emacs][zenburn]] theme.

#+BEGIN_SRC emacs-lisp
  (use-package zenburn-theme
    :init
    ;; use variable-pitch fonts for some headings and titles
    (setq zenburn-use-variable-pitch t)
    ;; scale headings in org-mode
    (setq zenburn-scale-org-headlines t)
    ;; scale headings in outline-mode
    (setq zenburn-scale-outline-headlines t)
    :config
    (load-theme 'zenburn t))
#+END_SRC
* Keybindings
  [[https://github.com/noctuid/general.el][=general.el=]] provides a more convenient method fork binding keys in
  emacs. I found [[https://sam217pa.github.io/2016/09/23/keybindings-strategies-in-emacs/][this article]] very useful to undersdand =general.el=
  with =evil= mode enabled.

  #+begin_src emacs-lisp
    (use-package general
      :config (general-evil-setup))
  #+end_src

  I'm binding my leader key to =SPC=, and =M-SPC= when not in normal mode.

  #+begin_src emacs-lisp
    (general-create-definer my-leader-def
      :prefix "SPC"
      :non-normal-prefix "M-SPC")
  #+end_src

  I'd like to simulate spacemacs =SPC m= and =,= binding to the major
  mode keybindings. But I'm too lazy to curate it so I'm converting
  those two sequences to =C-c=.

  #+begin_src emacs-lisp
    (general-create-definer my-local-leader-def
      :prefix "C-c")

    (general-define-key
     :states '(normal visual)
     "," (general-simulate-key "SPC m"))
  #+end_src

** My global keybinding map

   This is the central configuration for my keybindings.

  #+begin_src emacs-lisp
    (my-leader-def
      :states '(normal visual insert emacs)
      "SPC" '(counsel-M-x :which-key "M-x")
      "m" (general-simulate-key "C-c")
      "TAB" '(ivy-switch-buffer :which-key "prev buffer")
      ";" '(comment-or-uncomment-line-or-region :wk "comment")

      ;;; file prefix
      "f" '(:ignore t :wk "files prefix")
      "f f" 'find-file
      "f D" '((lambda() (interactive) (delete-file buffer-file-name)) :wk "delete")
      "f R" '((lambda() (interactive) (nolith/rename-current-buffer-file)) :wk "rename")
      "f e d" (lambda() (interactive) (find-file nolith/emacs-config-file))

      ;;; buffer prefix
      "b" '(:ignore t :wk "buffer prefix")
      "b b" 'switch-to-buffer
      "b s" '((lambda() (interactive) (switch-to-buffer "*scratch*")) :wk "scratch")
      "b m" '((lambda() (interactive) (switch-to-buffer "*Messages*")) :wk "Messages")
      "b d" '((lambda() (interactive) (kill-buffer (current-buffer))) :wk "kill current buffer")
      "b x w" 'delete-trailing-whitespace
      "b k" 'kill-buffer

      ;;; apps prefix
      "a" '(:ignore t :wk "apps prefix")
      "a g" '(:ignore t :wk "grammarly")
     )
  #+end_src

  #+begin_src emacs-lisp
    (general-iemap "C-;" 'comment-or-uncomment-line-or-region)
  #+end_src

** Other changes
   #+begin_src emacs-lisp
     (my-local-leader-def
      :states 'normal
      :keymaps 'with-editor-mode-map
      "c" 'with-editor-finish
      "k" 'with-editor-cancel)
   #+end_src

*** TODO Refactor this section

* Packages
** asdf
   #+begin_src emacs-lisp
	  (use-package asdf
	    :straight (asdf :type git :host github :branch "main" :repo "tabfugnic/asdf.el")
	    :config
		   (asdf-enable))
   #+end_src

** org-mode
   org-mode is at the heart of this config.

   In the configuration section we call
   =org-babel-do-load-languages= to declare which languages are
   allowed for org-babel execution.

   #+begin_src emacs-lisp
     (let ((straight-current-profile 'pinned))
       (use-package org
	 :straight (:branch "main")
	 :config
	 (my-local-leader-def
	   :states 'normal
	   :keymaps 'org-src-mode-map
	   "c" 'org-edit-src-exit
	   "k" 'org-edit-src-abort)
	 (org-babel-do-load-languages
	  'org-babel-load-languages
	  '((emacs-lisp . t)
	    (ruby . t)
	    (sqlite .t)
	    (shell . t))))
       ;; Pin org-mode version 9.4.4 "2b1fc6ba722ec4c8303f2fcfe33691012df8b1c2"
       (add-to-list 'straight-x-pinned-packages
		    '("org" . "2b1fc6ba722ec4c8303f2fcfe33691012df8b1c2")))
   #+end_src

** undo-fu
   Belive it or not, evil mode does not come with redo enabled!

#+begin_src emacs-lisp
  (use-package undo-fu
    :config
    (global-unset-key (kbd "C-z"))
    (global-set-key (kbd "C-z")   'undo-fu-only-undo)
    (global-set-key (kbd "C-S-z") 'undo-fu-only-redo))
#+end_src

** evil
  I've been a long time =vim= user, only =spacemacs= allowed me to switch to =emacs=.

  =evil-mode= implements ViM editing mode in emacs.

  #+BEGIN_SRC emacs-lisp
    (use-package evil
      :after undo-fu
      :init
      (setq evil-want-keybinding nil) ;; evil-collection requires this
      (setq evil-undo-function 'undo-fu)
      :config
      (define-key evil-motion-state-map "," nil) ;; unbind ,
      (evil-mode 1))
  #+END_SRC

  I often make use of [[https://github.com/emacs-evil/evil-surround][evil-surround]]

  #+BEGIN_SRC emacs-lisp
  (use-package evil-surround
    :config
    (global-evil-surround-mode 1))
  #+END_SRC

  [[https://github.com/redguardtoo/evil-matchit][evil-matchit]] for jumping to matched tags pressing =%=

  #+begin_src emacs-lisp
    (use-package evil-matchit
      :config
      (global-evil-matchit-mode 1))
  #+end_src

  [[https://github.com/emacs-evil/evil-collection][=evil-collection=]] is a collection of =evil= bindings for the parts
  of Emacs that Evil does not cover properly by default.

  #+begin_src emacs-lisp
    (use-package evil-collection
      :after evil
      :config
      (evil-collection-init))
  #+end_src

*** TODO Configure [[https://github.com/GuiltyDolphin/org-evil][org-evil]]

** magit

  It's a kind of magit.

  #+BEGIN_SRC emacs-lisp
    (let ((straight-current-profile 'pinned))
      (use-package magit
	:config
	(my-leader-def
	  :states '(normal visual insert emacs)
	  "g" 'magit-dispatch
	  "f g b" 'magit-blame-addition
	  "f g a" 'magit-stage-file)
	(setq magit-blame-styles '((headings
				    (heading-format . "%-20a %C %s
    "))
				   (margin
				    (margin-format " %s%f" " %C %a" " %H")
				    (margin-width . 42)
				    (margin-face . magit-blame-margin)
				    (margin-body-face magit-blame-dimmed))
				   (highlight
				    (highlight-face . magit-blame-highlight))
				   (lines
				    (show-lines . t)
				    (show-message . t))))
	(setq magit-blame-echo-style 'margin)
	(setq magit-refresh-status-buffer nil))
      ;; Pin magit v 3.3.0 - f44f6c14500476d918e9c01de8449edb20af4113
      (add-to-list 'straight-x-pinned-packages
		   '("magit" . "f44f6c14500476d918e9c01de8449edb20af4113")))
  #+END_SRC

  To increase [[https://magit.vc/manual/magit/Performance.html][magit performance]] I set =(setq magit-refresh-status-buffer nil)=

** ivy
  [[https://github.com/abo-abo/swiper][Ivy]] is a generic completion mechanism for Emacs, I was using helm in
  spacemacs but I'd like to try something new.

  #+BEGIN_SRC emacs-lisp
  (use-package ivy
    :config
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq enable-recursive-minibuffers t)
    (global-set-key "\C-s" 'swiper)
    (global-set-key (kbd "C-c C-r") 'ivy-resume)
    (global-set-key (kbd "<f6>") 'ivy-resume)
    (global-set-key (kbd "C-c g") 'counsel-git)
    (global-set-key (kbd "C-c j") 'counsel-git-grep)
    (global-set-key (kbd "C-c k") 'counsel-ag)
    (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
    )
  #+END_SRC

  Counsel is part of Ivy, it will replace some common functions, like
  ='find-file= with one powered by Ivy

  #+BEGIN_SRC emacs-lisp
    (use-package counsel
      :after ivy
      :config
      (my-leader-def
	:states '(normal visual insert emacs)
	"/" 'counsel-ag)
      (counsel-mode 1))
  #+END_SRC

** wgrep
   [[https://github.com/mhayashi1120/Emacs-wgrep][wgrep]] allows you to edit a grep buffer and apply those changes to
   the file buffer like sed interactively.

   This is needed to have the same feature as =helm-ag= with =ivy=.

   This was found on [[https://sam217pa.github.io/2016/09/11/nuclear-power-editing-via-ivy-and-ag/][Nuclear weapon multi-editing via Ivy and Ag]].

   Workflow:

   - =C-c k= search with =ag=
   - =C-c C-o= (=ivy-occur=) open rearch results in a buffer
   - =C-x C-q= (=ivy-wgrep-change-to-wgrep-mode=) to edit it
   - =C-c C-c= (=wgrep-finish-edit=) to edit (without saving) all the buffers

   #+begin_src emacs-lisp
     (use-package wgrep)
   #+end_src

** which-key
   There's still a lot to learn for me in Emacs, keybindings discoverability
   is a must.

   #+BEGIN_SRC emacs-lisp
   (use-package which-key
     :config
     (which-key-mode))
   #+END_SRC

** projectile
   I work a monay projects, Projectile gives me project scoped
   functions using Git repositories as project root.

   [[https://docs.projectile.mx/projectile/index.html][Projectile documentation]]

   #+BEGIN_SRC emacs-lisp
     (use-package projectile
       :init
       (projectile-mode +1)
       :config
       (general-define-key
	:keymaps '(normal insert emacs)
	:prefix "SPC"
	:non-normal-prefix "C-c"
	"p" 'projectile-command-map))
   #+END_SRC

** Company mode
   [[https://company-mode.github.io/][Company-mode]] is a text completion system.

   #+begin_src emacs-lisp
     (use-package company
       :init
       (add-hook 'after-init-hook 'global-company-mode))
   #+end_src

   [[https://github.com/dunn/company-emoji][company-emoji]] adds emoji support to company mode 🎉

   I had to override default branch to =trunk=, detecting default
   banch seems to be now supported in =straight.el= master branch,
   but I don't think it's already released ⏳.

   #+begin_src emacs-lisp
     (use-package company-emoji
       :straight (:branch "trunk")
       :after company
       :init
       (defun --set-emoji-font (frame)
	 "Adjust the font settings of FRAME so Emacs can display emoji properly."
	 (if (eq system-type 'darwin)
	     ;; For NS/Cocoa
	     (set-fontset-font t 'symbol (font-spec :family "Apple Color Emoji") frame 'prepend)
	   ;; For Linux
	   (set-fontset-font t 'symbol (font-spec :family "Symbola") frame 'prepend)))
       :config
       (add-to-list 'company-backends 'company-emoji)
       ;; For when Emacs is started in GUI mode:
       (--set-emoji-font nil)
       ;; Hook for when a frame is created with emacsclient
       ;; see https://www.gnu.org/software/emacs/manual/html_node/elisp/Creating-Frames.html
       (add-hook 'after-make-frame-functions '--set-emoji-font))
   #+end_src

** CSV-mode

  CSV editing

  #+BEGIN_SRC emacs-lisp
    (use-package csv-mode)
  #+END_SRC

** jsonnet

   #+begin_src emacs-lisp
     (straight-use-package `(jsonnet-mode :type git :branch "main"))
   #+end_src

** Markdown
*** markdown-mode

    I write a lot of markdown, let's make it comforable.

    #+begin_src emacs-lisp
      (use-package markdown-mode
	:mode (("\\.md\\'" . gfm-mode)
	       ("\\.markdown\\'" . markdown-mode))
	:init (setq markdown-command "multimarkdown"))
    #+end_src
*** vmd-mode
    Sometimes I like having markdown live preview. [[https://github.com/blak3mill3r/vmd-mode][=vmd-mode]] if the
    right package for the job.

    #+begin_src emacs-lisp
      (use-package vmd-mode
	:config
	(my-local-leader-def
	  :keymaps 'markdown-mode-map
	  "l" '(vmd-mode :wk "live preview")))
    #+end_src

    This package depends on [[https://github.com/yoshuawuyts/vmd][=vmd=]]

    #+begin_src shell
      npm install -g vmd
    #+end_src

**** TODO add a keybindings to =markdown-mode=

** Go
    Go programming language is supported by =go-mode= package.

    #+begin_src emacs-lisp
      (use-package go-mode
	:hook (
	       (before-save . gofmt-before-save)))
    #+end_src

** platformio

    #+begin_src emacs-lisp
      (use-package platformio-mode
	:after lsp-mode
	:config
	;; Enable ccls for all c++ files, and platformio-mode only
	;; when needed (platformio.ini present in project root).
	(add-hook 'c++-mode-hook (lambda ()
				   (lsp-deferred)
				   (platformio-conditionally-enable))))
    #+end_src

** C/C++ with =ccls=

   The language server configuration is described [[https://github.com/MaskRay/ccls/wiki/lsp-mode][inside =ccls= wiki]].

   #+begin_src emacs-lisp
     (use-package ccls
       :after lsp-mode
       :config
       (setq c-basic-offset 4)
       (setq c-file-style "bsd")
       :hook ((c-mode objc-mode cuda-mode) .   ;; c++-mode removed as it's part of platformio-mode
	      (lambda () (require 'ccls) (lsp-deferred))))
   #+end_src

** PHP

   Install [[https://github.com/emacs-php/php-mode][php-mode]].

   #+begin_src emacs-lisp
     (use-package php-mode)
   #+end_src

** Hex files - =nhexl-mode=
   This package implements NHexl mode, a minor mode for editing files
   in hex dump format.  The mode command is called `nhexl-mode'.

   This minor mode implements similar functionality to `hexl-mode',
   but using a different implementation technique, which makes it
   usable as a "plain" minor mode.  It works on any buffer, and does
   not mess with the undo log or with the major mode.

   It also comes with:

   - `nhexl-nibble-edit-mode': a "nibble editor" minor mode.
     where the cursor pretends to advance by nibbles (4-bit) and the
     self-insertion keys let you edit the hex digits directly.
   - `nhexl-overwrite-only-mode': a minor mode to try and avoid moving text.
     In this minor mode, not only self-inserting keys overwrite existing
     text, but commands like `yank' and `kill-region' as well.
   - it overrides C-u to use hexadecimal, so you can do C-u a 4 C-f
     to advance by #xa4 characters.

   Even though the hex addresses and hex data displayed by this mode aren't
   actually part of the buffer's text (contrary to hexl-mode, for example,
   they're only added to the display), you can search them with Isearch,
   according to nhexl-isearch-hex-addresses and nhexl-isearch-hex-bytes.

#+begin_src elisp
  (use-package nhexl-mode)
#+end_src

** exec-path-from-shell

   Starting emacs from MacOS launcher will result in a different
   =$PATH= from what I'm using on my shell, [[https://github.com/purcell/exec-path-from-shell][=exec-path-from-shell]] will
   fix that.

   #+begin_src emacs-lisp
     (use-package exec-path-from-shell
       :config
       ;; only on MacOS or linux X system
       (when (memq window-system '(mac ns x))
	 (exec-path-from-shell-initialize))
       ;; or when launched as a deamon from systemd
       (when (daemonp)
	 (exec-path-from-shell-initialize)))
   #+end_src

** flycheck
   [[https://www.flycheck.org/en/latest/][Flycheck]] provides syntax checking for Emacs.

   #+begin_src emacs-lisp
     (use-package flycheck
       :init (global-flycheck-mode))
   #+end_src

** Language Server Protocol
   [[https://emacs-lsp.github.io/lsp-mode/][lsp-mode]] provides a language server protocol client implementation for emacs.

   #+begin_src emacs-lisp
     (use-package lsp-mode
       :init (setq lsp-keymap-prefix "C-c l")
       :hook (
	      (ruby-mode . lsp)
	      (go-mode . lsp)
	      ;; if you want which-key integration
	      (lsp-mode . lsp-enable-which-key-integration))
       :commands (lsp lsp-deferred))
   #+end_src

   [[https://emacs-lsp.github.io/lsp-mode/page/languages/][Language servers]] must be installed separately.

   Let's install a couple ef extensions

   #+begin_src emacs-lisp
     (use-package lsp-ui :commands lsp-ui-mode)
     (use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
   #+end_src

*** grammarly

    [[https://github.com/emacs-grammarly/lsp-grammarly][lsp-grammarly]] provides a grammarly language server.

    #+begin_src emacs-lisp
      (use-package lsp-grammarly
	:unless (eq system-type 'windows-nt)
	:after keytar
	:config
	(setq lsp-grammarly-auto-activate nil) ;; don't start automatically
	(setq lsp-grammarly-dialect "american")
	(setq lsp-grammarly-audience "knowledgeable")
	(setq lsp-grammarly-domain "technical")
	(setq lsp-grammarly-active-modes
	      '(text-mode latex-mode org-mode markdown-mode))
	(add-to-list 'lsp-language-id-configuration '(text-mode . "grammarly"))
	(add-to-list 'lsp-language-id-configuration '(latex-mode . "grammarly"))
	(add-to-list 'lsp-language-id-configuration '(org-mode . "grammarly"))
	(add-to-list 'lsp-language-id-configuration '(markdown-mode . "grammarly"))
	(add-to-list 'lsp-language-id-configuration '(gfm-mode . "grammarly"))
	(general-define-key
	 :keymaps '(normal)
	 :prefix "SPC a g"
	 "s" 'lsp-grammarly-stop
	 "c" 'lsp-grammarly-check-grammar
	 "i" 'lsp-grammarly-get-document-state)
	:straight (lsp-grammarly :type git :host github :repo "emacs-grammarly/lsp-grammarly"))
	;; :hook (text-mode . (lambda ()
	;; 		       (require 'lsp-grammarly)
	;; 		       (lsp-deferred))))
    #+end_src

    =lsp-grammarly= requires [[https://github.com/emacs-grammarly/keytar][=keytar=]] to handle passwords.


    #+begin_src emacs-lisp
      (use-package keytar
	:straight (keytar :type git :host github :repo "emacs-grammarly/keytar"))
    #+end_src

*** TODO take a look at treemacs
   #+begin_src emacs-lisp :tangle no
     (use-package lsp-treemacs :commands lsp-treemacs-errors-list)
   #+end_src

*** TODO take a look at =dap-mode=, the debugger
    =dap-mode= is the debugger integration.

   #+begin_src emacs-lisp :tangle no
     ;; optionally if you want to use debugger
     (use-package dap-mode)
     ;; (use-package dap-LANGUAGE) to load the dap adapter for your language
   #+end_src

** powerline
   I like a less cluttered powerline.

   #+begin_src emacs-lisp
     (use-package powerline
       :config (powerline-center-evil-theme))
   #+end_src

** yasnippet

   [[https://github.com/joaotavora/yasnippet][YASnippet]] is a template system for emacs.

   It comes without snippets, but there is a good collection in
   [[https://github.com/AndreaCrotti/yasnippet-snippets][yasnippet-snippets]], on top of that I'm setting my personal snippets
   collection on this repo.

   #+begin_src emacs-lisp
     (use-package yasnippet
       :config
       (setq yas-snippet-dirs
	     '("~/src/literate-dotfiles/emacs/snippets"))
       (use-package yasnippet-snippets)
       (yas-global-mode 1))
   #+end_src

** atomic-chrome
   [[https://github.com/alpha22jp/atomic-chrome][atomic-chrome]] creates a bi-directional channel between emacs and
   the browser using [[https://github.com/GhostText/GhostText][GhostText]] browser extension.

   #+begin_src emacs-lisp
     (use-package atomic-chrome
       :config
       (setq atomic-chrome-default-major-mode 'markdown-mode)
       (setq atomic-chrome-url-major-mode-alist
	   '(("github\\.com" . gfm-mode)
	     ("gitlab\\.com" . gfm-mode)))
       (setq atomic-chrome-server-ghost-text-port 4001) ;; default
       (atomic-chrome-start-server))
   #+end_src

   =atomic-chrome-default-major-mode= defaults to =textual-mode=, but
   I usually write in markdown, even where it's not supported 😂

   =atomic-chrome-url-major-mode-alist= is for setting the major mode
   based on the page URL.

   =atomic-chorome-server-ghost-text-port= must match the port
   configured in GhostText.

** fish shell
   My shell if =fish=, so I need =fish-mode=.

   #+begin_src emacs-lisp
     (use-package fish-mode)
   #+end_src

** yaml-mode
   I write yaml file daily.

   #+begin_src emacs-lisp
     (use-package yaml-mode)
   #+end_src

** vterm

   A performat terminal inside emacs

   #+begin_src emacs-lisp
     (use-package vterm
       :unless (eq system-type 'windows-nt)
       :config
       (setq vterm-buffer-name-string "vterm %s")
       :ensure t)
   #+end_src
*** Shell configuration

    Some of the most useful features in vterm (e.g.,
    directory-tracking and prompt-tracking or message passing) require
    shell-side configurations. The main goal of these additional
    functions is to enable the shell to send information to vterm via
    properly escaped sequences. A function that helps in this task,
    vterm_printf, is defined below. This function is widely used
    throughout this readme.

    #+begin_src fish :tangle ~/.config/fish/functions/vterm_printf.fish :mkdirp yes
      function vterm_printf;
	  if begin; [  -n "$TMUX" ]  ; and  string match -q -r "screen|tmux" "$TERM"; end
	      # tell tmux to pass the escape sequences through
	      printf "\ePtmux;\e\e]%s\007\e\\" "$argv"
	  else if string match -q -- "screen*" "$TERM"
	      # GNU screen (screen, screen-256color, screen-256color-bce)
	      printf "\eP\e]%s\007\e\\" "$argv"
	  else
	      printf "\e]%s\e\\" "$argv"
	  end
      end
    #+end_src

    #+begin_src fish :tangle ~/.config/fish/functions/fish_title.fish :mkdirp yes
      function fish_title
	  hostname
	  echo ":"
	  pwd
      end
    #+end_src

    #+begin_src fish :tangle ~/.config/fish/functions/vterm_clear.fish :mkdirp yes
      if [ "$INSIDE_EMACS" = 'vterm' ]
	  function clear
	      vterm_printf "51;Evterm-clear-scrollback";
	      tput clear;
	  end
      end
    #+end_src

** generic-mode
   =generic-mode= is a catchall mode to define new major modes for simple sintaxes.

   More details on [[https://www.emacswiki.org/emacs/GenericMode][Emacs Wiki]].

   #+begin_src emacs-lisp
     (require 'generic-x)
   #+end_src

*** GCode

    This is a very simple attempt to define a major mode for the [[https://marlinfw.org/meta/gcode/][Marlin firmware GCode]].

    #+begin_src emacs-lisp
      (define-generic-mode gcode-marlin-mode
	'(";")
	nil
	'(("\\bS{\\([A-Z_a-z0-9]+\\)}" 1 font-lock-variable-name-face)
	 ("\\b\\([XYZESFPLBIT]\\)" 1 font-lock-type-face)
	 ("\\b[GM][0-9]+\\b" . font-lock-keyword-face)
	 ("-?[0-9]+\\(\\.[0-9]+\\)?\\b" . font-lock-constant-face))
	'("\\.gcode\\'")
	nil
	"Generic mode for 3D printer marlin g-code files.")
    #+end_src
