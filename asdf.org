#+TITLE:      ASDF  configuration
#+AUTHOR:     Alessio Caiazza
#+KEYWORDS:   Alessio Caiazza asdf
#+STARTUP:    showall
#+PROPERTY:   header-args:fundamental :tangle ~/.asdfrc
#+PROPERTY:   header-args:fundamental+ :comment both :mkdirp yes
-----

* Ignore patterns
:PROPERTIES:
:header-args: :comments both :mkdirp yes
:END:

** =asdf.rc=
:PROPERTIES:
:header-args+: :tangle ~/.asdfrc
:END:

   This is the general configuration file.

   Not all my projects uses =.tool-versions=, many legacy projects still have legacy version files.

   #+begin_src conf
     legacy_version_file = yes
   #+end_src

** ruby
:PROPERTIES:
:header-args+: :tangle ~/.default-gems
:END:

   It is possible to define ruby gems to be installed on every new ruby installation.

   I need =solargraph= for the language server to work properly.
   #+begin_src conf
	    solargraph
   #+end_src

   I'm using lefthook, and I want each ruby version to have it installed.
   #+begin_src conf
	    lefthook
   #+end_src

** python
:PROPERTIES:
:header-args+: :tangle ~/.default-python-packages
:END:

   It is possible to define python packages to be installed on every new python installation.

   I need =pipenv= for the deployer.
   #+begin_src conf
	    pipenv
   #+end_src
