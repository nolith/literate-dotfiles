;; package --- machine-identifier.el

;;; Commentary:

;; help me identify machines

;;; Code:

(straight-use-package 'uuidgen)
(require 'uuidgen)

;;; Variables

(defvar-local nolith/default-config
  '(git-email "376774-nolith@users.noreply.gitlab.com" gpg-signature "")
  "Default configuration.")

(defvar-local nolith/config '()
  "The configuration for this machine.")

(defvar-local nolith/machine 'unknown
  "The machine identifier symbol.")

(defvar-local nolith/net-interfaces '("en0" "eth0")
  "The list of network interfaces to get the MAC address from.")

(defvar-local nolith/machines '("8c85901c92ab" kitsune "3c22fb6932be" ram)
  "The list of known machines.")

;;; Machine identification

(let ((uuidgen-interface (catch 'net-interface
			 (dolist (interface nolith/net-interfaces)
			   (unless (equal (network-interface-info interface) nil)
			     (throw 'net-interface interface)))))
      (config (list 'ram '(git-email "acaiazza@gitlab.com" gpg-signature "D9FC0A382ED14A911459DB99BBC5AAAAD498F0F5") 'kitsune '(git-email "nolith@abisso.org"))))

  (defconst nolith/machine-identifier (uuidgen--format-ieee-address))

  (let ((machine (lax-plist-get nolith/machines nolith/machine-identifier)))
    (if (eq machine nil)
	(message "Unknown machine - please define it!")
      (progn
	(setq-local nolith/machine machine)
	(setq-local nolith/config (plist-get config nolith/machine)))))
  (message "Machine: %s" nolith/machine)

  (defvar nolith/config (plist-get config nolith/machine)))

;;; Public functions

(defun nolith/get-config (key)
  "Fetch the KEY configuration value for the current machine."
  (let ((value (plist-member nolith/config key)))
    (if (eq value nil)
	(plist-get nolith/default-config key)
      (cadr value))))

(provide 'machine-identifier)
;;; machine-identifier.el ends here
