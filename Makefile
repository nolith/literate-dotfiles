mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))
cache_dir := .cache
publish_dir := public
timestamps_dir := .timestamps
docs := README.org sitemap.org
orgs := $(filter-out $(docs), $(shell git ls-files \*.org))
emacs_pkgs := org

init_el := elisp/init.el

^el = $(filter %.el,$^)
EMACS.funcall = emacs --batch --no-init-file --load $(init_el) --funcall
SED ?= $(shell which gsed || which sed)

all: publish tangle

test: tangle

publish: $(init_el) $(orgs) public/_redirects
	$(EMACS.funcall) literate-dotfiles-publish-all
	$(SED) -i -e 's|^<body>|<body class="h-entry">|'\
		-e 's|^<h1 class="title">|<h1 class="title p-title">|'\
		-e 's|^<main |<main class="e-content p-name" |'\
		public/*.html

docker_publish: $(init_el) $(orgs) .config/emacs/
	docker run --rm \
		-v $(mkfile_dir):/data/ \
		alpine:3.12 /bin/sh -c "cd /data && apk add -U make git emacs-nox && make publish"

docker_tangle:  $(init_el) $(orgs) .config/emacs/
	docker run --rm \
		-v $(mkfile_dir):/data/ \
		alpine:3.12 /bin/sh -c "cd /data && apk add -U make git emacs-nox && make tangle"

clean:
	rm -rf $(publish_dir)
	rm -rf $(timestamps_dir)
	rm -rf $(cache_dir)

tangle: $(basename $(orgs))

# https://emacs.stackexchange.com/a/27128/2780
$(cache_dir)/%.out: %.org $(init_el) $(cache_dir)/
	$(EMACS.funcall) literate-dotfiles-tangle $< > $@

$(cache_dir)/emacs/%.out: emacs/%.org $(init_el) $(cache_dir)/emacs/
	$(EMACS.funcall) literate-dotfiles-tangle $< > $@

public/_redirects: $(cache_dir)/gitlab-pages.out

%/:
	mkdir -p $@

$(cache_dir)/emacs/:
	mkdir -p $@

%: %.org $(cache_dir)/%.out
	@$(NOOP)

emacs/%: emacs/%.org $(cache_dir)/emacs/%.out
	@$(NOOP)

git: emacs_pkgs = gitconfig-mode gitattributes-mode gitignore-mode

.PHONY: all clean
.PRECIOUS: $(cache_dir)/ $(cache_dir)/%.out
